import React, { Component } from 'react';
import './Calc.css';

function MessageIMC(result) {
  if (result < 18) return 'Você está abaixo do peso';
  else if (result < 24) return 'Você está no peso normal';
  else if (result < 30) return 'Você está acima do peso';
  else if (result > 30) return 'Tome cuidado, você está obeso';
}

class Calc extends Component {
  constructor(props) {
    super(props);
    this.state = {
      peso: 0,
      altura: 0,
      valor: '',
    };

    this.handlePesso = this.handlePesso.bind(this);
    this.handleAltura = this.handleAltura.bind(this);
    this.clickCalcIMC = this.clickCalcIMC.bind(this);
  }

  handlePesso(event) {
    this.setState({ peso: event.target.value });
  }

  handleAltura(event) {
    this.setState({ altura: event.target.value });
  }

  clickCalcIMC() {
    const { peso, altura } = this.state;
    let valor = peso / (altura * altura);
    this.setState({ valor });
  }

  render() {
    const valor = this.state.valor;
    const result = Number.isNaN(parseFloat(valor)) ? '0' : valor;
    const messageIMC = MessageIMC(result);
    return (
      <div className="calc">
        <label htmlFor="peso">Peso: </label>
        <input
          type="text"
          onChange={this.handlePesso}
          id="peso"
          placeholder="Seu peso..."
          autoFocus
        />
        <label htmlFor="altura">Altura: </label>
        <input
          type="text"
          onChange={this.handleAltura}
          id="altura"
          placeholder="Sua altura..."
        />
        <button onClick={this.clickCalcIMC}>Calcular IMC</button>
        {valor > 0 && (
          <p>
            <strong>IMC: </strong>
            {parseFloat(result).toFixed(2)}
          </p>
        )}
        {result > 0 && <p className="mensagem">{messageIMC}</p>}
      </div>
    );
  }
}
export default Calc;
